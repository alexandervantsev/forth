' dup g" 
( a - a a )
Duplicate the cell on top of the stack.
" doc-word 

' drop g"
( a -- )
Drop the topmost element of the stack
" doc-word

' swap g" 
( a b -- b a )
Swap two topmost elements of the stack
" doc-word

' rot g" 
( a b c -- b c a )
Third element of the stack becomes the first
" doc-word

' + g" 
( x y -- [ x + y ] )
Add two topmost elements of the stack up
" doc-word

' * g" 
( x y -- [ x * y ] )
Multiply two topmost elements of the stack
" doc-word

' / g" 
( x y -- [ x / y ] )
Divide second element of the stack by the first
" doc-word

' % g" 
( x y -- [ x mod y ] )
Take the remainder of of division of the second element by the first
" doc-word

' - g" 
( x y -- [x - y] )
Deduct the first element from the second one
" doc-word

' < g" 
( x y -- [x < y] )
Compare two topmost elements of the stack, return 1 if true else return 0
" doc-word

' not g" 
( a -- a' ) a' = 0 if a != 0 a' = 1 if a == 0
Return 0 if the topmost element is not 0, else return 1
" doc-word

' = g" 
( a b -- c ) c = 1 if a == b c = 0 if a != b
Compare two topmost elements, return 1 if they are equal, else return 0
" doc-word

' land g" 
( a b -- a && b ) Logical and
If a != 0 and b != 0 return a, else return 0
" doc-word

' lor g" 
( a b -- a || b ) Logical or
If a != 0 or b != 0 return any non-zero element of these two, else return 0
" doc-word