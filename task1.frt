: if-even 2 % if ." not even" else ." even" then ;

: inc 1 + ;

: if-prime dup 1 = if else  
    dup 2 = if drop 1 else
	0 swap
	dup 2 - 2 do 
	    dup r@  % if else 
		swap inc swap then
	loop
	r@  % if else inc then
	if 0 else 1 
	then
    then
then ;

: prm-ans if ." prime" else ." not prime" ;

: prm-allot 1 allot swap over ! ;

: concat swap over over count swap count 1 + + heap-alloc
    over count 0 
    do over r@ + c@ over r@ + c! loop 
    over count + >r swap r> over count 0 
    do over r@ + c@ over r@ + c! loop  
    over count over + 0 swap c!  
    >r heap-free dup count swap heap-free r> swap - ; 


: radical 1 swap dup 2 
    do
        r@ if-prime if dup r@ % if else swap r@ * swap else then then
    loop 
    drop ; 


